const path = require('path');
const fs = require('fs');
const got = require('got');
const parser = require('fast-xml-parser');

const dotenv = require('dotenv');

dotenv.config('../.env');

const rawDb = require('./db/raw');
const { agrs, hrtime, pino } = require('./utils');

const options = {
  attributeNamePrefix: '@_',
  attrNodeName: 'attr', // default is 'false'
  textNodeName: '#text',
  ignoreAttributes: true,
  ignoreNameSpace: false,
  allowBooleanAttributes: false,
  parseNodeValue: true,
  parseAttributeValue: false,
  trimValues: true,
  cdataTagName: '__cdata', // default is 'false'
  cdataPositionChar: '\\c',
  localeRange: '', // To support non english character in tag/attribute values.
  parseTrueNumberOnly: false,
};
const t1 = process.hrtime();
const logger = pino({ name: 'agoda-content' }, path.resolve(__dirname, '../logs/agoda-content.log'));

const getPropertyCodeFromFile = () => {
  const { f } = agrs();
  if (!f) throw new Error('File name not valid');
  const file = fs.readFileSync(path.resolve(f));
  let fileStr = file.toString();
  fileStr = fileStr.replace(/"/g, '');
  fileStr = fileStr.replace(/'/g, '');
  // Need to improve for complex csv
  const propertyCodes = fileStr.split(/\r?\n/);
  return propertyCodes;
};

/**
 * @description : get data from API call and save these data to database.
 * @param {*} queryClient : query instance for postgresql
 * @param {*} instance  : call instance for eps API.
 * @param {*} lang : language query
 * @param {*} propertyIds : property codes from csv file.
 * @param {*} curIdx : current index
 */
function writeDataToDatabase(queryClient, instance, propertyId) {
  return new Promise((resolve) => {
    const apiKey = process.env.AGODA_API_KEY;
    const feedId = process.env.AGODA_FEED_ID;
    const localId = 1;

    (async () => {
      try {
        const resp = await instance(`/datafeeds/feed/getfeed?apikey=${apiKey}&mhotel_id=${propertyId}&feed_id=${feedId}`);
        let resData = resp.body;

        if (resData) {
          if (parser.validate(resData) === true) {
            resData = parser.parse(resData, options);
          }
          const hotelData = resData.Hotel_feed_full;
          const point = {};
          if (hotelData && hotelData.hotels && hotelData.hotels.hotel
            && hotelData.hotels.hotel.longitude && hotelData.hotels.hotel.latitude) {
            point.type = 'Point';
            point.coordinates = [hotelData.hotels.hotel.latitude, hotelData.hotels.hotel.longitude];
          }
          if (hotelData && hotelData.hotels && hotelData.hotels.hotel
              && hotelData.hotels.hotel.hotel_id) {
            const propertyCode = propertyId; // hotelData.hotels.hotel.hotel_id;
            const supplierCode = 'agoda';
            let rawData = { [propertyCode]: hotelData };
            rawData = JSON.stringify(rawData);
            rawData = rawData.replace(/'/g, "''");
            const query = `INSERT INTO "public"."supplier_properties" ("property_code", "supplier_code", "locale_id", "raw_data", "coordinate", "active") \
                              VALUES ('${propertyCode}', '${supplierCode}', ${localId}, '${rawData}'::jsonb, \
                                    ST_SetSRID(ST_MakePoint((${point.coordinates[1]})::float,(${point.coordinates[0]})::float),4326), true)\
                              ON CONFLICT (supplier_code, property_code, locale_id) \
                              DO UPDATE SET \
                                  "property_code"='${propertyCode}', \
                                  "supplier_code"='${supplierCode}', \
                                  "locale_id"=${localId}, \
                                  "raw_data"='${rawData}'::jsonb, \
                                  "coordinate"=ST_SetSRID(ST_MakePoint((${point.coordinates[1]})::float,(${point.coordinates[0]})::float),4326), \
                                  "active"=true \
                              WHERE ("public"."supplier_properties"."property_code" = '${propertyCode}')`;
            await queryClient.query(query).then(() => {
              logger.info({ msg: 'query is runned successfully.', propertyCode: propertyId });
              resolve(true);
            })
              .catch((err) => {
                logger.error({ msg: 'Errored query', q: query, dur: hrtime(process.hrtime(t1)).ms });
                if (queryClient) {
                  queryClient.release(err);
                }
                resolve(false);
                // throw err;
              });
          } else {
            logger.error({ msg: 'Errored Hotel ID', propertyCode: propertyId, dur: hrtime(process.hrtime(t1)).ms });
            resolve(true);
          }
        } else {
          logger.error({ msg: 'Empty Property', propertyCode: propertyId, dur: hrtime(process.hrtime(t1)).ms });
          resolve(true);
        }
      } catch (err) {
        logger.error({ msg: 'Error Request', Response: err.response.body, dur: hrtime(process.hrtime(t1)).ms });
        resolve(true);
      }
    })();
  });
}

function sendMultipleRequest(queryInst, Inst, propertyCodes, curIdx) {
  const promises = [];
  const limitCnt = process.env.AGODA_BATCH_CNT;
  let currentIdx = curIdx;
  const toCnt = parseInt((currentIdx), 10) + parseInt((limitCnt), 10);
  const end = (propertyCodes.length > toCnt) ? toCnt : parseInt(propertyCodes.length, 10);
  for (let i = currentIdx; i < end; i += 1) {
    promises.push(writeDataToDatabase(queryInst, Inst, propertyCodes[i]));
  }
  Promise.all(promises).then((res) => {
    let resp = true;
    for (let i = 0; i < res.length; i += 1) {
      if (res[i] === false) {
        resp = false;
        break;
      }
    }
    if (resp === true) {
      currentIdx = end;
      if (currentIdx < propertyCodes.length) {
        setTimeout(() => {
          sendMultipleRequest(queryInst, Inst, propertyCodes, currentIdx);
        }, 3000);
      } else if (queryInst) {
        queryInst.release();
        logger.info('finished!!!');
      }
    } else {
      logger.error('response is not match');
    }
  });
}

const start = async () => {
  logger.info('Started!!!');

  const queryClient = await rawDb.getClient();
  const propertyCodes = getPropertyCodeFromFile();
  const hostName = process.env.AGODA_API_HOST;
  const gotInst = got.extend({
    baseUrl: hostName,
  });
  if (propertyCodes.length > 0) {
    sendMultipleRequest(queryClient, gotInst, propertyCodes, 0);
  } else {
    logger.error({ msg: 'Property Code is empty.', dur: hrtime(process.hrtime(t1)).ms });
  }
};

start()
  .catch((e) => logger.error(e.stack));
