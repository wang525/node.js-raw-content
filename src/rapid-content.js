const path = require('path');
const fs = require('fs');
const CryptoJS = require('crypto-js');
const got = require('got');
const dotenv = require('dotenv');

dotenv.config('../.env');
const rawDb = require('./db/raw');
const { agrs, hrtime, pino } = require('./utils');

const t1 = process.hrtime();
const logger = pino({ name: 'rapid-content' }, path.resolve(__dirname, '../logs/rapid-content.log'));

const getPropertyCodeFromFile = () => {
  const { f } = agrs();
  if (!f) throw new Error('File name not valid');
  const file = fs.readFileSync(path.resolve(f));
  // Need to improve for complex csv
  const propertyCodes = file.toString().split(/\r?\n/);
  return propertyCodes;
};

const getHeader = () => {
  const apiKey = process.env.EPS_API_KEY;
  const secret = process.env.EPS_API_SECRET;

  const timestamp = Math.round((new Date().getTime() / 1000));
  const hash = CryptoJS.SHA512(apiKey + secret + timestamp).toString(CryptoJS.enc.Hex);
  const authHeaderValue = `EAN APIKey=${apiKey},Signature=${hash},timestamp=${timestamp}`;

  const res = {
    Accept: 'application/json',
    'Accept-Encoding': 'gzip',
    Authorization: authHeaderValue,
    'User-Agent': 'TravelNow/3.30.112',
  };
  return res;
};

/**
 * @description : get data from API call and save these data to database.
 * @param {*} queryClient : query instance for postgresql
 * @param {*} instance  : call instance for eps API.
 * @param {*} lang : language query
 * @param {*} propertyIds : property codes from csv file.
 * @param {*} curIdx : current index
 * @param {*} localId : local Id from locales table.
 */
function writeDataToDatabase(queryClient, instance, lang, propertyIds, curIdx, localId) {
  const limitCnt = parseInt(process.env.EPS_API_QUERY_LIMIT, 10);
  let currentIdx = curIdx;
  let propertyQueries = '';
  const toCnt = parseInt((currentIdx), 10) + parseInt((limitCnt), 10);
  // eslint-disable-next-line max-len
  const end = (propertyIds.length > toCnt) ? toCnt : parseInt(propertyIds.length, 10);
  for (let i = currentIdx; i < end; i += 1) {
    propertyQueries += `&property_id=${propertyIds[i]}`;
  }
  currentIdx = end;

  (async () => {
    try {
      const resp = await instance(`/properties/content?language=${lang}${propertyQueries}`);

      const resData = resp.body;
      // Recevied data convert to Array.
      const arrayRes = Object.keys(resData).map((key) => (
        { propertyId: key, content: resData[key] }
      ));

      if (arrayRes && arrayRes.length > 0) {
        arrayRes.map(async (item) => {
          const point = {};
          if (item && item.content && item.content.location && item.content.location.coordinates) {
            const coord = item.content.location.coordinates;
            point.type = 'Point';
            point.coordinates = [coord.latitude, coord.longitude];
          }
          const propertyCode = item.propertyId;
          const supplierCode = 'expedia_rapid';
          let rawData = { [item.propertyId]: item.content };
          rawData = JSON.stringify(rawData);
          // eslint-disable-next-line no-useless-escape
          rawData = rawData.replace(/\'/g, "''");
          const query = `INSERT INTO "public"."supplier_properties" ("property_code", "supplier_code", "locale_id", "raw_data", "coordinate", "active") \
                        VALUES ('${propertyCode}', '${supplierCode}', ${localId}, '${rawData}'::jsonb, \
                                ST_SetSRID(ST_MakePoint((${point.coordinates[1]})::float,(${point.coordinates[0]})::float),4326), true)\
                        ON CONFLICT (supplier_code, property_code, locale_id) \
                        DO UPDATE SET \
                            "property_code"='${propertyCode}', \
                            "supplier_code"='${supplierCode}', \
                            "locale_id"=${localId}, \
                            "raw_data"='${rawData}'::jsonb, \
                            "coordinate"=ST_SetSRID(ST_MakePoint((${point.coordinates[1]})::float,(${point.coordinates[0]})::float),4326), \
                            "active"=true \
                        WHERE ("public"."supplier_properties"."property_code" = '${propertyCode}')`;
          await queryClient.query(query).then(() => {
            logger.info('query is runned successfully.');
          })
            .catch((err) => {
              logger.error({ msg: 'Errored query', q: query, dur: hrtime(process.hrtime(t1)).ms });
              if (queryClient) {
                queryClient.release(err);
              }
              throw err;
            });
        });
      }

      // In the case that data is more than limit count. call API again
      if (currentIdx < propertyIds.length) {
        setTimeout(() => {
          writeDataToDatabase(queryClient, instance, lang, propertyIds, currentIdx, localId);
        }, 3000);
      } else if (queryClient) {
        queryClient.release();
      }
    } catch (err) {
      logger.error({ msg: 'Error Request', Response: err.response.body, dur: hrtime(process.hrtime(t1)).ms });
    }
  })();
}

const start = async () => {
  logger.info('Started!!!');

  const { l } = agrs();
  const queryClient = await rawDb.getClient();
  const propertyCodes = getPropertyCodeFromFile();
  const header = getHeader();

  const hostName = process.env.EPS_API_HOST;
  const gotInst = got.extend({
    baseUrl: hostName,
    json: true,
    headers: header,
  });

  let localId;
  const query = `SELECT id FROM locales WHERE code='${l}'`;
  await queryClient.query(query).then((result) => {
    if (result && result.rows && result.rows.length === 1) {
      localId = result.rows[0].id;
    } else {
      logger.warn('This language query is not unique. Please check the database.');
    }
  })
    .catch((err) => {
      logger.error({ msg: 'Errored query', q: query, dur: hrtime(process.hrtime(t1)).ms });
      if (queryClient) {
        queryClient.release(err);
      }
      throw err;
    });

  writeDataToDatabase(queryClient, gotInst, l, propertyCodes, 0, localId);
};

start()
  .catch((e) => logger.error(e.stack));
