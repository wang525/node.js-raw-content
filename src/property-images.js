/* eslint-disable class-methods-use-this */
const path = require('path');
const fs = require('fs');
const { Writable } = require('stream');


const QueryStream = require('./utils/pg-query-batch-stream');
const { agrs, hrtime, pino } = require('./utils');

const zeusDb = require('./db/zeus');
const rawDb = require('./db/raw');

const insertPropertyImages = fs.readFileSync(path.resolve(__dirname, './sql/insert-property-images.sql'));

const logger = pino({ name: 'property-images' }, path.resolve(__dirname, '../logs/property-images.log'));

const getPropertyCodeFromFile = () => {
  const { f } = agrs();
  if (!f) throw new Error('File name not valid');
  const file = fs.readFileSync(path.resolve(f));
  // Need to improve for complex csv
  const propertyCodes = file.toString().split('\n');
  return propertyCodes;
};

const start = async () => {
  logger.info('started');
  const queryClient = await rawDb.getClient();
  const propertyCodes = getPropertyCodeFromFile();
  const fakeString = propertyCodes.map((ele, idx) => `$${idx + 2}`);
  logger.info({
    msg: 'Parsed proeprty codes from selected file',
    data: {
      propertyRequested: propertyCodes.length,
    },
  });
  const getRapidFromRawSupplierWithEanCode = {
    text: `select ean_id, $1::text as supplier_code, caption, hero_image::text, links from expedia_rapid_images as raw where raw.rapid_code in (${fakeString.join(',')})`,
    values: ['expedia_ean', ...propertyCodes],
    rowMode: 'array',
    // values: ['expedia_ean', ''568576','591482','778160','721966','144184','614616','468227','128909','627879','468426',']
  };

  const streamQuery = new QueryStream(getRapidFromRawSupplierWithEanCode.text,
    getRapidFromRawSupplierWithEanCode.values,
    // Control how many paraller connection to DB, check ur pool size limit
    { batchSize: 5 });

  const tT = process.hrtime();
  const stream = queryClient.query(streamQuery);

  class InsertStream extends Writable {
    _write(chunk, encoding, cb) {
      Promise.all(chunk.map(async (ele) => {
        // Multiple connection to DB.
        const t1 = process.hrtime();
        const query = {
          text: insertPropertyImages.toString(),
          values: [...Object.values(ele)],
        };
        let client;
        try {
          client = await zeusDb.getClient();
          await client.query(query);
          logger.debug({
            msg: 'excuted query', q: query.text, dur: hrtime(process.hrtime(t1)).ms,
          });
          client.release();
          return client;
        } catch (err) {
          logger.error({
            msg: 'Errored query', q: query.text, dur: hrtime(process.hrtime(t1)).ms,
          });
          if (client) {
            client.release(err);
          }
          throw err;
        }
      }))
        .then(() => cb())
        .catch((err) => cb(err));
    }
  }

  const insertStrem = new InsertStream({
    objectMode: true,
  });


  insertStrem.on('drain', () => logger.warn('Waiting for availabel DB client'));
  insertStrem.on('finish', async () => {
    logger.info({ msg: 'Sucessfully insert all entries to DB', dur: hrtime(process.hrtime(tT)).s });
    await rawDb.pool.end();
    logger.info('Drained pool');
    // process.kill(process.pid, 'SIGTERM');
  });
  insertStrem.on('error', async (err) => {
    logger.error(err);
    await rawDb.pool.end();
    // throw err;
  });

  stream.pipe(insertStrem);
  stream.on('end', () => queryClient.release());
};

process.on('SIGTERM', () => {
  logger.info('SIGTERM');
});

start()
  .catch((e) => logger.error(e.stack));
