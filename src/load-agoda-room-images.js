const path = require('path');
const fs = require('fs');

const dotenv = require('dotenv');

dotenv.config('../.env');

const rawDb = require('./db/raw');
const zeusDb = require('./db/zeus');
const { agrs, hrtime, pino } = require('./utils');

const t1 = process.hrtime();
const logger = pino({ name: 'agoda-room-images' }, path.resolve(__dirname, '../logs/agoda-room-images.log'));

const supplierCode = 'agoda';

const getCodeFromFile = () => {
  const { f } = agrs();
  if (!f) throw new Error('File name not valid');
  const file = fs.readFileSync(path.resolve(f));
  let fileStr = file.toString();
  fileStr = fileStr.replace(/"/g, '');
  fileStr = fileStr.replace(/'/g, '');
  let codeValues = {};
  const propertyCods = [];
  const roomCodes = [];
  // Need to improve for complex csv
  const lineCodes = fileStr.toString().split(/\r?\n/);
  if (lineCodes && lineCodes.length > 1) {
    const codeName = lineCodes[0].split(/,/);
    for (let i = 1; i < lineCodes.length; i += 1) {
      const tmpCode = lineCodes[i].split(/,/);
      propertyCods.push(`'${tmpCode[0]}'`);
      roomCodes.push(`'${tmpCode[1]}'`);
    }
    codeValues = { [codeName[0]]: propertyCods, [codeName[1]]: roomCodes };
  } else {
    logger.error({ msg: 'Error Codes Input', Code: lineCodes, dur: hrtime(process.hrtime(t1)).ms });
  }
  return codeValues;
};

function getExtensionAndSize(url) {
  // get the extension and size of the image.
  const splitUrls = url.split('?');
  let res = { e: ' ', s: 0 };

  if (splitUrls && splitUrls.length > 0) {
    const tmpUrl = splitUrls[0].split('.');
    const tmpSize = splitUrls[1].split('=');

    if (tmpUrl && tmpSize && tmpUrl.length > 0 && tmpSize.length > 0) {
      const ext = `.${tmpUrl[tmpUrl.length - 1]}`;
      const size = parseInt(tmpSize[1], 10);
      res = { e: ext, s: size };
    } else {
      logger.error({ msg: 'Image URL is not correct.', u: url, dur: hrtime(process.hrtime(t1)).ms });
    }
  } else {
    logger.error({ msg: 'Image URL is not correct.', u: url, dur: hrtime(process.hrtime(t1)).ms });
  }
  return res;
}

const saveDataToZeus = async (data) => {
  const zeusDbClient = await zeusDb.getClient();
  let query = 'INSERT INTO "public"."rooms" ("details", "code", "property_code", "descriptions", "beds", "active", "supplier_code") VALUES ';
  let imageValueQuery = '';

  for (let i = 0; i < data.length; i += 1) {
    let { details } = data[i];
    const analyzeVal = getExtensionAndSize(data[i].hotel_room_type_picture);
    details = JSON.stringify(details);
    details = details.replace(/'/g, "''");
    query += `('${details}'::jsonb, '${data[i].code}', '${data[i].property_code}', '${data[i].description}', '${data[i].beds}', true, '${supplierCode}')`;
    query += (i < data.length - 1) ? ', ' : '';

    imageValueQuery += `('${data[i].hotel_room_type_picture}', '${data[i].description}', '${analyzeVal.e}', 'GET', 
                          (select code from image_sizes where max_w > ${analyzeVal.s} and min_w <= ${analyzeVal.s}),
                          't', true, '${supplierCode}', '${data[i].property_code}', '${data[i].code}')`;
    imageValueQuery += (i < data.length - 1) ? ', ' : '';
  }
  query += `ON CONFLICT (code, property_code, supplier_code) DO UPDATE SET 
            ("details", "code", "property_code", "descriptions", "beds", "active", "supplier_code")=
            (excluded."details", 
            excluded."code", 
            excluded."property_code", 
            excluded."descriptions", 
            excluded."beds", 
            excluded."active", 
            excluded."supplier_code") RETURNING *;\n`;

  // insert image_size to room_images table.
  query += `INSERT INTO "public"."room_images" 
              ("url", "description", "extension", "method", "size_code", "hero", "active", "supplier_code", 
              "property_code", "room_code") VALUES `;
  query += imageValueQuery;
  query += `ON CONFLICT (room_code, property_code, supplier_code) DO UPDATE SET 
              ("url", "description", "extension", "method", "size_code", "hero", "active", "supplier_code", 
              "property_code", "room_code")=
              (excluded."url", 
              excluded."description", 
              excluded."extension", 
              excluded."method", 
              excluded."size_code", 
              excluded."hero", 
              excluded."active", 
              excluded."supplier_code", 
              excluded."property_code",
              excluded."room_code"
              ) RETURNING *;`;

  await zeusDbClient.query(query).then(() => {
    logger.info({ msg: 'query in zeus DB is runned successfully.' });
    if (zeusDbClient) {
      zeusDbClient.release();
    }
  })
    .catch((err) => {
      logger.error({ msg: 'Errored query in zeus DB', q: err, dur: hrtime(process.hrtime(t1)).ms });
      if (zeusDbClient) {
        zeusDbClient.release(err);
      }
    });
};

const start = async () => {
  logger.info('Started!!!');

  const rawDbClient = await rawDb.getClient();
  const codeValues = getCodeFromFile();

  if (codeValues && codeValues.property_code && codeValues.property_code.length > 0
    && codeValues.room_code && codeValues.room_code.length > 0) {
    const query = `select 
                  json_build_object('view', k.views, 'max_pax', k.max_occupancy_per_room, 'size_of_room', k.size_of_room, 'max_extrabeds', k.max_extrabeds) as details, 
                  k.standard_caption as description, 
                  k.hotel_id as property_code, 
                  k.hotel_room_type_id as code, 
                  k.hotel_room_type_picture, 
                  k.bed_type as beds, 
                  k.hotel_master_room_type_id 
                  from supplier_properties 
                  inner join LATERAL ( 
                  select * from 
                  jsonb_to_recordset(raw_data -> property_code -> 'roomtypes' -> 'roomtype') as 
                    x(views text, bed_type text, 
                      hotel_id int, no_of_room int, 
                      max_extrabeds int, 
                      size_of_room float, 
                      standard_caption text, 
                      hotel_room_type_id int, 
                      max_infant_in_room int, 
                      max_occupancy_per_room int, 
                      room_size_incl_terrace boolean, 
                      hotel_room_type_picture text, 
                      hotel_master_room_type_id int, 
                      standard_caption_translated text, 
                      hotel_room_type_alternate_name text)
                  ) as k on true
                  where 
                  supplier_code = '${supplierCode}' 
                  and property_code in (${codeValues.property_code}) 
                  and k.hotel_room_type_id in (${codeValues.room_code});`;

    await rawDbClient.query(query).then((res) => {
      logger.info({ msg: 'query in raw DB is runned successfully.' });
      if (res && res.rows && res.rows.length > 0) {
        const hotelData = res.rows;
        if (rawDbClient) {
          rawDbClient.release();
        }
        saveDataToZeus(hotelData);
      } else {
        logger.info({ msg: 'No such data in raw DB Database :', codeValues, dur: hrtime(process.hrtime(t1)).ms });
        if (rawDbClient) {
          rawDbClient.release();
        }
      }
    })
      .catch((err) => {
        logger.error({ msg: 'Errored query', q: query, dur: hrtime(process.hrtime(t1)).ms });
        if (rawDbClient) {
          rawDbClient.release(err);
        }
      });
  } else {
    logger.error({ msg: 'Property code is empty or not correct.', code: codeValues, dur: hrtime(process.hrtime(t1)).ms });
    if (rawDbClient) {
      rawDbClient.release();
    }
  }
};

start()
  .catch((e) => logger.error(e.stack));
