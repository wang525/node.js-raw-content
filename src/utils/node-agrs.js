/**
 * This a helper to extrac node agrs from cli
 * e.i. node run-me.js -f=./home/readme.md
 * output { f: './home/readme.md' }
 * it always return string, and only support -f=xxxx.js format
 *
 * In future we might want to consider 3rd party npm package
 */

module.exports = () => {
  // Remove fist 2 node js default values
  const rest = process.argv.slice(2);

  // will be return later
  const args = {};

  rest.forEach((ele) => {
    const reg = new RegExp('-(.*)=(.*)', 'ig');
    const o = reg.exec(ele);
    const [, key, val] = o;
    if (!key || !val) throw new Error('Invalid aguratment');
    args[key] = val;
  });

  return args;
};
