const hrtime = require('./convert-hrtime');
const pino = require('./logger');
const agrs = require('./node-agrs');

module.exports = {
  agrs,
  hrtime,
  pino,
};
