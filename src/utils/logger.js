const path = require('path');
const pino = require('pino');

/**
 * Just a wrapper of PINO, with a indention preety print during development
 * Destination logging in production.
 */
module.exports = (opts, relativeLogPath) => {
  const isProduction = process.env.NODE_ENV === 'production';

  const logPath = path.resolve(__dirname, relativeLogPath);
  const dest = isProduction ? pino.destination(logPath) : null;
  const prettyPrint = !isProduction ? { prettyPrint: { colorize: true } } : {};

  return pino({
    ...prettyPrint,
    base: null,
    ...opts,
  },
  dest);
};
