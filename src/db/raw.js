
const fs = require('fs');
const { Pool } = require('pg');

const convertHrtime = require('../utils/convert-hrtime');
const pino = require('../utils/logger');


const logger = pino({ name: 'raw-db' }, '../../logs/db.log');

// const caCertPath = path.resolve(__dirname, process.env.RAW_CA_CERT_PATH);
const caCertString = fs.readFileSync(process.env.RAW_CA_CERT_PATH).toString();

const config = {
  database: process.env.RAW_DB_DATABASE,
  host: process.env.RAW_DB_HOST,
  user: process.env.RAW_DB_USER,
  port: process.env.RAW_DB_PORT,
  password: process.env.RAW_DB_PASSWORD,
  ssl: {
    ca: caCertString,
  },
  max: 100,
  idleTimeoutMillis: 10000,
  connectionTimeoutMillis: 2000,
};

const pool = new Pool(config);

pool.on('connect', () => logger.debug({ msg: 'New pool client connected' }));
pool.on('acquire', () => logger.debug({ msg: 'Acuqired a exiting client' }));
pool.on('remove', () => logger.info({ msg: 'Client removed from pool' }));
pool.on('error', (err) => logger.error({ msg: err.msg, err }));

module.exports = {
  pool,
  query: async (text, params) => {
    const start = process.hrtime();
    return new Promise((resolve, reject) => {
      pool.query(text, params, (err, res) => {
        const texta = typeof text !== 'string' ? text.text : text;
        const rowMode = typeof text !== 'string' ? text.rowMode : undefined;

        const duration = convertHrtime(process.hrtime(start));
        if (!err) {
          logger.debug({
            msg: 'excuted query', text: texta, rowMode, duration: duration.ms, rows: res.rowCount,
          });
          resolve(res);
        } else {
          logger.error({
            msg: 'Errored query', text, duration: duration.ms, err,
          });
          reject(err);
        }
      });
    });
    // try {
    //   const res = await pool.query(text, params);
    //   logger.info({ msg: 'excuted query', text, duration, rows: res.rowCount });
    //   return res;
    // } catch (err) {
    //   logger.error({ msg: 'Errored query', text, duration, err });
    //   throw err;
    // }
    // return pool.query(text, params, (err, res) => {
    //   const duration = convertHrtime(process.hrtime(start));
    //   if (!err) {
    //     logger.info({ msg: 'excuted query', text, duration, rows: res.rowCount });
    //     return res;
    //   } else {
    //     logger.error({ msg: 'Errored query', text, duration, err });
    //     throw err;
    //   }
    // })
  },
  // getClient: async () => new Promise(async (resolve, reject) => {
  //   try {
  //     const c = await pool.connect();
  //     logger.debug('RAW DB CREATED POOL COONECTION');
  //     resolve(c);
  //   } catch (err) {
  //     logger.error({ msg: 'ERROR ON CREATE POOL COONECTION', err });
  //     reject(err);
  //   }
  // }),
  async getClient() {
    try {
      const c = await pool.connect();
      logger.debug('RAW DB CREATED POOL COONECTION');
      return c;
    } catch (err) {
      logger.error({ msg: 'ERROR ON CREATE POOL COONECTION', err });
      throw err;
    }
  },
};
