const fs = require('fs');
const youtubedl = require('youtube-dl');

const start = async () => {
  console.log('started!!!');
  const video = youtubedl('http://www.youtube.com/watch?v=fieUOGI0wnw', // Optional arguments passed to youtube-dl.
    ['--format=18'], // Additional options can be given for calling `child_process.execFile()`.
    { cwd: __dirname });

  // Will be called when the download starts.
  video.on('info', (info) => {
    console.log('Download started');
    console.log('filename: ' + info._filename);
    console.log('size: ' + info.size);
  });

  video.pipe(fs.createWriteStream('myvideo.mp3'));
};

start()
  .catch((e) => {
    console.log('start error:', e.stack);
  });
