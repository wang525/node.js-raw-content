-- This sql is use conjunction with prepare statement
-- First insert new property image into property_image can get the pkey
with new_property_img as (
  insert into property_images (property_code, supplier_code, description, hero, active, inserted_at, updated_at)
  values($1, $2, $3, $4, true, now(), now())
  returning id as pkey
)
-- By returning pkey, we insert image url by using pkey returned to link it together
insert into property_image_urls (property_image_id, size_code, url, method, extension)
  select new_property_img.pkey, size, val ->> 'href', 'GET', 'jpg'
  from
    json_each($5) as t(size, val)
    inner join new_property_img on TRUE
  where val is not null;