This project contains a fews achoc script to increase productivity.

## property-images
We connected 2 to databases, zeus and digital ocean. It query data with the property ID define in `testfile.csv`, and update Zeus `property-images` and `property-image-urls` tables by batches.

You can control the batch size also.

```
NODE_ENV=production node src/property-images.js -f=testfile.csv
```

To see the log `logs/property-images.log`

This project contains a fews achoc script to increase productivity.

## rapid-content
Download rapid-content and save to digitalocean

```
NODE_ENV=production node src/rapid-content.js -f=rapdicode.csv
```

To see the log `logs/rapid-content.log`

```
To run for production
NODE_ENV=production node src/rapid-content.js -f=testfile.csv -l=en-US

To run for agoda API production
NODE_ENV=production node src/agoda-content.js -f=testfile2.csv
