module.exports = {
  "extends": "airbnb-base",
  "rules": {
    "no-unused-vars": ["error", { "argsIgnorePattern": "^_", "ignoreRestSiblings": false }],
    "no-underscore-dangle": "off",
    "linebreak-style": ["error", "windows"]
  }
};